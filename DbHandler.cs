﻿using cars_data_thief.Model;
using MicroLite;
using MicroLite.Configuration;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace cars_data_thief.Tools
{
    public class DbHandler
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private ISessionFactory _sessionFactory;
        public static DbHandler Instance = new DbHandler();

        public DbHandler()
        {
            Configure.Extensions().WithAttributeBasedMapping();
            _sessionFactory = Configure.Fluently().ForMsSql2012Connection("CarsDB", Configuration.ConnectionString, "System.Data.SqlClient").CreateSessionFactory();
        }

        public bool TestConnection()
        {
            try
            {
                logger.Info("Testing connection...");

                using (var session = _sessionFactory.OpenSession())
                using (var transaction = session.BeginTransaction())
                {
                    List<Car> Cars = session.Fetch<Car>(new SqlQuery("SELECT TOP 3 * FROM Cars")).ToList();
                }

                logger.Info("Success!");
                return true;
            }
            catch(Exception e)
            {
                logger.Fatal("Can`t establish connection! Check your db settings");
                logger.Fatal(e);

                return false;
            }
        }

        public bool DoBackup()
        {
            try
            {
                logger.Info("Trying to do backup...");

                List<string> files = new List<string>();

                if (!Directory.Exists(Configuration.BackupsDir))
                    Directory.CreateDirectory(Configuration.BackupsDir);
                else
                {
                    files = Directory.GetFiles(Configuration.BackupsDir).ToList();

                    for (int i = 0; i < files.Count; i++)
                    {
                        if ((File.GetCreationTime(files[i]).Date - DateTime.Now.Date).Days == 0)
                        {
                            logger.Info("Unnecessary to do backup, because it has been already done today");
                            return true;
                        }
                    }
                }
        
                string filePath = string.Format("{0}{1}_{2}.bak", Configuration.BackupsDir, "CarsDatabase", DateTime.Now.ToString("dd-MM-yyyy"));

                using (var connection = new SqlConnection(Configuration.ConnectionString))
                {
                    var query = String.Format("BACKUP DATABASE [{0}] TO DISK='{1}'", Configuration.DbName, filePath);

                    using (var command = new SqlCommand(query, connection))
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }

                logger.Info("Success!");

                if(files.Count > 4)
                {
                    logger.Info("Deleting old backups...");

                    List<string> filesToDelete = (from file in files
                                                  where File.GetCreationTime(file) == (from f in files select File.GetCreationTime(f)).Min()
                                                  select file).ToList();

                    for(int i = 0; i < filesToDelete.Count; i++)
                    {
                        File.Delete(filesToDelete[i]);
                    }

                    logger.Info("Success!");
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Fatal("Unable to do backup! Check your db settings");
                logger.Fatal(e);

                return false;
            }
        }

        public bool InsertCarToDB(Car car)
        {
            try
            {
                using (var session = _sessionFactory.OpenSession())
                using (var transaction = session.BeginTransaction())
                {
                    logger.Info("Trying to add car with mark = {0}, model = {1}, value = {2} to DB...", car.Mark, car.Model, car.Value);

                    var query = new SqlQuery("SELECT CarId FROM Cars WHERE Mark = @p0 AND Model = @p1 AND Value = @p2", car.Mark, car.Model, car.Value);

                    int car_id = session.Single<int>(query);
                    if (car_id == 0)
                    {
                        session.Insert(car);
                        transaction.Commit();

                        logger.Info("Car with mark = {0}, model = {1}, value = {2} was successfully added to DB!", car.Mark, car.Model, car.Value);
                        return true;
                    }  
                    else
                    {
                        logger.Error("Car with mark = {0}, model = {1}, value = {2} already exist in DB!", car.Mark, car.Model, car.Value);
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                logger.Info("Error while adding car with mark = {0}, model = {1}, value = {2} to DB!", car.Mark, car.Model, car.Value);
                logger.Error(e);

                return false;
            }
        }

        public bool InsertGoodToDB(Car car, ref Good good)
        {
            try
            {
                using (var session = _sessionFactory.OpenSession())
                using (var transaction = session.BeginTransaction())
                {
                    logger.Info("Trying to add good: {0}, {1}, {2}", good.Category, good.Name, good.Number);

                    var query = new SqlQuery("SELECT GoodId FROM Goods WHERE Category = @p0 AND Name = @p1 AND Number = @p2", good.Category, good.Name, good.Number);

                    int good_id = session.Single<int>(query);
                    if (good_id == 0)
                    {
                        session.Insert(good);
                        logger.Info("Good: {0}, {1}, {2} was successfully added to DB!", good.Category, good.Name, good.Number);
                        good_id = session.Single<int>(query);
                    }
                    else
                    {
                        logger.Info("Good: {0}, {1}, {2} already exist in DB! Updating...", good.Category, good.Name, good.Number);
                         
                        good.GoodId = good_id;
                        session.Update(good);
                        logger.Info("Success!");
                    }

                    logger.Info("Trying to add connection to good: {0}, {1}, {2}!", good.Category, good.Name, good.Number);

                    query = new SqlQuery("SELECT CarId FROM Cars WHERE Mark = @p0 AND Model = @p1 AND Value = @p2", car.Mark, car.Model, car.Value);


                    InsertCarToDB(car);
                    int car_id = session.Single<int>(query);

                    query = new SqlQuery("SELECT GoodToCarId FROM GoodToCar WHERE GoodId = @p0 AND CarId = @p1", good_id, car_id);
                    int good_to_car_id = session.Single<int>(query);
                    if (good_to_car_id == 0)
                    {
                        session.Insert(new GoodToCar() { GoodId = good_id, CarId = car_id });
                        logger.Info("Connection to good: {0}, {1}, {2} was succesfully added!", good.Category, good.Name, good.Number);
                    }
                    else
                    {
                        logger.Error("Connection to good: {0}, {1}, {2} already exist!", good.Category, good.Name, good.Number);
                    }


                    transaction.Commit();

                    return true;
                }
            }
            catch (Exception e)
            {
                logger.Info("Error while adding good: {0}, {1}, {2} to DB!", good.Category, good.Name, good.Number);
                logger.Error(e);

                return false;
            }
        }
    }
}