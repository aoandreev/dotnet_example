﻿using NLog;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using cars_data_thief.Model;

namespace cars_data_thief.Tools
{
    public class WebHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static FirefoxDriver serviceDriver;
        public static WebDriverWait serviceWait;

        public static FirefoxDriver parsingDriver;
        public static WebDriverWait parsingWait;

        public static bool InitWebDriver(out FirefoxDriver fdriver, out WebDriverWait wait)
        {
            try
            {
                logger.Info("Trying to init WebDriver...");

                fdriver = new FirefoxDriver();
                fdriver.Manage().Timeouts().PageLoad = TimeSpan.FromMinutes(5);
                fdriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);

                wait = new WebDriverWait(fdriver, TimeSpan.FromSeconds(20));

                logger.Info("Init was done successfully");

                return true;
            }
            catch (Exception e)
            {
                logger.Error("Unable to init web driver");
                logger.Error(e);
                fdriver = null;
                wait = null;
                return false;
            }
        }

        //Login to ecat
        public static bool Login(FirefoxDriver fdriver, WebDriverWait wait)
        {
            try
            {
                logger.Info("Trying to login...");
                
                fdriver.Navigate().GoToUrl(Configuration.Link);
                wait.Timeout = TimeSpan.FromSeconds(10);
                wait.Until(ExpectedConditions.ElementExists(By.Id("ctl00_ContentPlaceHolder1_LoginControl_tbLoginUser")));

                fdriver.FindElement(By.Id("ctl00_ContentPlaceHolder1_LoginControl_tbLoginUser")).Clear();
                fdriver.FindElement(By.Id("ctl00_ContentPlaceHolder1_LoginControl_tbLoginUser")).SendKeys(Configuration.Login);
                Thread.Sleep(4000);
                fdriver.FindElement(By.Id("ctl00_ContentPlaceHolder1_LoginControl_tbLoginPass")).Clear();
                fdriver.FindElement(By.Id("ctl00_ContentPlaceHolder1_LoginControl_tbLoginPass")).SendKeys(Configuration.Password);

                Thread.Sleep(2000);
                wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("ctl00_ContentPlaceHolder1_LoginControl_btnLogin")));
                fdriver.FindElement(By.Id("ctl00_ContentPlaceHolder1_LoginControl_btnLogin")).Click();

                Thread.Sleep(5000);

                logger.Info("Success!");
                return true;
            }
            catch (Exception e)
            {
                logger.Error("Unable to login");
                logger.Error(e);
                return false;
            }
        }

        public static bool GotoCarsList(FirefoxDriver fdriver, WebDriverWait wait)
        {
            try
            {
                logger.Info("Trying to go to cars list...");

                wait.Timeout = TimeSpan.FromSeconds(10);
                wait.Until(ExpectedConditions.ElementExists(By.ClassName("sub-menu-item-unselected-first")));

                fdriver.Navigate().GoToUrl("https://ecat.ua/CarBrowser.aspx?TTab=False");

                Thread.Sleep(3000);
                wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("ctl00_cphBody_CarBrowserCtl_lbBrands")));

                return true;
            }
            catch(Exception e)
            {
                logger.Error("Unable to go to cars list");
                logger.Error(e);

                return false;
            }
        }

        public static bool GetMarks(FirefoxDriver fdriver)
        {
            try
            {
                logger.Info("Trying to get marks list");
                Program.form.ListBoxModels.DataSource = null;
                Program.form.ListBoxCars.DataSource = null;

                Program.form.ListBoxMarks.DisplayMember = "Title";
                Program.form.ListBoxMarks.ValueMember = "Value";
                Program.form.ListBoxMarks.DataSource =
                    (from comp in fdriver.FindElement(By.Id("ctl00_cphBody_CarBrowserCtl_lbBrands")).FindElements(By.TagName("option"))
                     select new { Title = comp.GetAttribute("title"), Value = comp.GetAttribute("value") }).ToList();

                logger.Info("Marks list was gotten succesfully");

                return true;
            }
            catch (Exception e)
            {
                logger.Error("Unable to get marks list");
                logger.Error(e);

                return false;
            }
        }

        public static bool GetModels(FirefoxDriver fdriver, WebDriverWait wait)
        {
            try
            {
                logger.Info("Trying to get models list");
                Program.form.ListBoxCars.DataSource = null;

                wait.Timeout = TimeSpan.FromSeconds(20);
                
                var element = (from el in fdriver.FindElement(By.Id("ctl00_cphBody_CarBrowserCtl_lbBrands")).FindElements(By.TagName("option"))
                              where el.GetAttribute("value").Equals(Program.form.ListBoxMarks.SelectedValue)
                              select el).ToList();

                element[0].Click();
                Thread.Sleep(5000);
                wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("ctl00_cphBody_CarBrowserCtl_lbModels")));
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id("ctl00_cphBody_CarBrowserCtl_UpdateProgress1")));

                Program.form.ListBoxModels.DisplayMember = "Title";
                Program.form.ListBoxModels.ValueMember = "Value";
                Program.form.ListBoxModels.DataSource = 
                    (from comp in fdriver.FindElement(By.Id("ctl00_cphBody_CarBrowserCtl_lbModels")).FindElements(By.TagName("option"))
                     select new { Title = comp.GetAttribute("title").Remove(comp.GetAttribute("title").IndexOf("[")).Trim(),
                         Value = comp.GetAttribute("value") }).ToList();

                logger.Info("Models list was gotten succesfully");

                return true;
            }
            catch (Exception e)
            {
                logger.Error("Unable to get models list");
                logger.Error(e);

                return false;
            }
        }

        public static bool GetCars(FirefoxDriver fdriver, WebDriverWait wait)
        {
            try
            {
                logger.Info("Trying to get cars list");

                wait.Timeout = TimeSpan.FromSeconds(20);

                var element = (from el in fdriver.FindElement(By.Id("ctl00_cphBody_CarBrowserCtl_lbModels")).FindElements(By.TagName("option"))
                               where el.GetAttribute("value").Equals(Program.form.ListBoxModels.SelectedValue)
                               select el).ToList();

                element[0].Click();
                Thread.Sleep(5000);
                wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("ctl00_cphBody_CarBrowserCtl_lbTypes")));
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id("ctl00_cphBody_CarBrowserCtl_UpdateProgress1")));

                Program.form.ListBoxCars.DisplayMember = "Title";
                Program.form.ListBoxCars.ValueMember = "Value";
                Program.form.ListBoxCars.DataSource = 
                    (from comp in fdriver.FindElement(By.Id("ctl00_cphBody_CarBrowserCtl_lbTypes")).FindElements(By.TagName("option"))
                     select new { Title = comp.GetAttribute("title"), Value = comp.GetAttribute("value") }).ToList();

                logger.Info("Cars list was gotten succesfully");

                return true;
            }
            catch (Exception e)
            {
                logger.Error("Unable to get cars");
                logger.Error(e);

                return false;
            }
        }

        //Parsing basic cars data
        public static bool ParseCars(FirefoxDriver fdriver, WebDriverWait wait)
        {
            try
            {
                if (!GotoCarsList(fdriver, wait)) return false;

                logger.Info("Parsing marks, models and types...");
                wait.Timeout = TimeSpan.FromSeconds(20);
               
                List<IWebElement> list_marks = fdriver.FindElement(By.Id("ctl00_cphBody_CarBrowserCtl_lbBrands")).FindElements(By.TagName("option")).ToList();
                int marksCount = list_marks.Count;

                //Main cicle of car parsing
                for (int i = 0; i < marksCount; i++)
                {
                    string mark_title = list_marks[i].GetAttribute("title").Trim();
                    string mark_value = list_marks[i].GetAttribute("value").Trim();

                    //History sync marks
                    if (History.Instance.Mark.Equals("-")) History.Instance.Mark = mark_value;
                    if (!History.Instance.Mark.Equals(mark_value)) continue;
                    else
                    {
                        list_marks[i].Click();
                        Thread.Sleep(5000);
                        wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("ctl00_cphBody_CarBrowserCtl_lbModels")));
                        wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id("ctl00_cphBody_CarBrowserCtl_UpdateProgress1")));

                        List<IWebElement> list_models = fdriver.FindElement(By.Id("ctl00_cphBody_CarBrowserCtl_lbModels")).FindElements(By.TagName("option")).ToList();
                        int modelsCount = list_models.Count;

                        for (int j = 0; j < modelsCount; j++)
                        {
                            string model_title = list_models[j].GetAttribute("title").Remove(list_models[j].Text.IndexOf("[")).Trim();
                            string model_value = list_models[j].GetAttribute("value").Trim();

                            //History sync models
                            if (History.Instance.Model.Equals("-")) History.Instance.Model = model_value;
                            if (!History.Instance.Model.Equals(model_value)) continue;
                            else
                            {
                                list_models[j].Click();
                                Thread.Sleep(3000);
                                wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("ctl00_cphBody_CarBrowserCtl_lbTypes")));
                                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id("ctl00_cphBody_CarBrowserCtl_UpdateProgress1")));

                                List<IWebElement> list_types = fdriver.FindElement(By.Id("ctl00_cphBody_CarBrowserCtl_lbTypes")).FindElements(By.TagName("option")).ToList();

                                for (int k = 0; k < list_types.Count; k++)
                                {
                                    string type = list_types[k].GetAttribute("value").Trim();

                                    //History sync types
                                    if (History.Instance.Type.Equals("-")) History.Instance.Type = type;
                                    if (!History.Instance.Type.Equals(type)) continue;
                                    else
                                    {
                                        logger.Info("Parsing mark = {0}, model = {1}, value = {2}", mark_title, model_title, type);

                                        if (Common.ParseCarType(list_types[k].GetAttribute("title"), out Car car))
                                        {
                                            car.Mark = mark_title;
                                            car.Model = model_title;
                                            car.Value = type;

                                            logger.Info("Successfully parsed car with mark = {0}, model = {1}, value = {2}", mark_title, model_title, type);

                                            DbHandler.Instance.InsertCarToDB(car);

                                            list_types[k].Click();
                                            ParseGoods(fdriver, wait, car);

                                        }

                                        //History sync types
                                        History.Instance.Type = "-";
                                        list_types = fdriver.FindElement(By.Id("ctl00_cphBody_CarBrowserCtl_lbTypes")).FindElements(By.TagName("option")).ToList();
                                    }
                                }

                                //History sync models
                                History.Instance.Model = "-";
                                list_models = fdriver.FindElement(By.Id("ctl00_cphBody_CarBrowserCtl_lbModels")).FindElements(By.TagName("option")).ToList();   
                            }
                        }

                        //History sync marks
                        History.Instance.Mark = "-";
                        list_marks = fdriver.FindElement(By.Id("ctl00_cphBody_CarBrowserCtl_lbBrands")).FindElements(By.TagName("option")).ToList(); 
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Error("Unable to parse car data");
                logger.Error(e);

                return false;
            }
        }

        //Parsing goods info 
        public static bool ParseGoods(FirefoxDriver fdriver, WebDriverWait wait, Car car)
        {
            try
            {
                Thread.Sleep(2000);
                wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("ctl00_cphSB_SBC_jstree")));
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id("ctl00_cphBody_CarBrowserCtl_UpdateProgress1")));

                logger.Info("Trying to parse goods info...");

                var jsTree = (from comp in fdriver.FindElement(By.Id("ctl00_cphSB_SBC_jstree")).FindElement(By.ClassName("jstree-no-icons")).FindElements(By.TagName("li"))
                              select new { Id = comp.GetAttribute("id"), Name = comp.FindElement(By.TagName("a")).Text.Remove(comp.FindElement(By.TagName("a")).Text.LastIndexOf("(")).Trim() }).ToList();

                for(int i = 0; i < jsTree.Count; i++)
                {
                    //History sync types
                    if (History.Instance.Category.Equals("-")) History.Instance.Category = jsTree[i].Name;
                    if (!History.Instance.Category.Equals(jsTree[i].Name)) continue;
                    else
                    {
                        logger.Info("Loading node {0}...", jsTree[i].Name);

                        fdriver.FindElement(By.Id(jsTree[i].Id)).Click();
                        Thread.Sleep(2000);
                        wait.Until(ExpectedConditions.ElementExists(By.Id("ctl00_cphBody_ArticleBrowserCtl_apcPager_pnlPager")));
                        wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id("ctl00_cphBody_CarBrowserCtl_UpdateProgress1")));

                        var pages = (from page in fdriver.FindElements(By.ClassName("page-number"))
                                     select page.GetAttribute("id")).ToList();
                        pages.Insert(0, "ctl00_cphBody_ArticleBrowserCtl_apcPager_pageLink0");

                        for (int j = 0; j < pages.Count; j++)
                        {
                            logger.Info("Loading page {0}...", j + 1);

                            fdriver.FindElement(By.Id(pages[j])).Click();
                            Thread.Sleep(2000);
                            wait.Until(ExpectedConditions.ElementExists(By.Id("ctl00_cphBody_ArticleBrowserCtl_apcPager_pnlPager")));
                            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id("ctl00_cphBody_CarBrowserCtl_UpdateProgress1")));

                            var goodsTable = (from item in fdriver.FindElement(By.Id("ctl00_cphBody_ArticleBrowserCtl_lvParts_tblAdvancedBrowser")).FindElement(By.TagName("tbody")).FindElements(By.TagName("tr"))
                                              where item.GetAttribute("id").Contains("ctl00_cphBody_ArticleBrowserCtl_lvParts_Item")
                                              select item).ToList();

                            for (int k = 0; k < goodsTable.Count - 1; k += 2)
                            {
                                if (Common.ParseGood(goodsTable[k], out Good good))
                                {
                                    good.Category = jsTree[i].Name;
                                    good.Description = goodsTable[k + 1].FindElements(By.TagName("td"))[0].Text;

                                    //If price still 0
                                    if (good.PriceOnSite == 0) logger.Error("Error while price parsing: {0}, {1}, {2}!", good.Category, good.Name, good.Number);

                                    logger.Info("Successfully parsed good: {0}, {1}, {2}!", good.Category, good.Name, good.Number);
                                    DbHandler.Instance.InsertGoodToDB(car, ref good);
                                }
                            }
                        }

                        History.Instance.Category = "-";
                    }
                }

                return true;
            }
            catch (System.Exception e)
            {
                logger.Error("Error while parsing goods");
                logger.Error(e);

                return false;
            }
            finally
            {
                History.Instance.Category = "-";
                fdriver.Navigate().GoToUrl("https://ecat.ua/CarBrowser.aspx?TTab=False");

                Thread.Sleep(2000);
                wait.Until(ExpectedConditions.ElementExists(By.Id("ctl00_cphBody_CarBrowserCtl_rbsCommon_1")));
            }
        }             
    }
}
